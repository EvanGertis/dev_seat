//******************************************************************************
//******************************************************************************
//******************************************************************************


#include "lpLaser.h"
#include "prnPrint.h"
#include <ostream>
#include <iostream>
#include <fstream>
#include <math.h>
#include <stdio.h>



//******************************************************************************
//******************************************************************************
//******************************************************************************

namespace LP
{


lpLaser::lpLaser()
{

 
}

//******************************************************************************
//******************************************************************************
//******************************************************************************
// Open the connection to the ILD1420 laser distance sensor.

void LP::lpLaser::open()
{
   //******************************************************************************
   //******************************************************************************
   //******************************************************************************
   // Clear out log file.
   std::ifstream logfile("LOG.txt");
   if (logfile.good())
   {
      logfile.clear();
   }

   // Open the sensor for communication valid for ILD1420. 
   // See page 199, section 7.2.2 of Medaqlib.pdf.
   ERR_CODE mError = OpenSensorRS232(mSensor, "COM8");

   //******************************************************************************
   //******************************************************************************
   //******************************************************************************
   // Enable logging, pg 89 of MEDAQLib manual.
   mError = EnableLogging(mSensor, TRUE, 8, 64, "LOG.txt", TRUE, TRUE, 0);

   //******************************************************************************
   //******************************************************************************
   //******************************************************************************

   // Pass the command Set_Baudrate see page 389.
   mError = SetParameterString(mSensor, "S_Command", "Set_Baudrate");
   // Pass the parameter SP_SensorBaudrate to set the baud rate see page 389.
   mError = SetParameterInt(mSensor, "SP_SensorBaudrate", mBaudrate);
   // Send the parameter string to the IF2001 module.
   mError = SensorCommand(mSensor);
   mError = GetError(mSensor, mErrorText, sizeof(mErrorText));
   Prn::print(Prn::ProcRun1,"Error %s", mErrorText);


   //******************************************************************************
   //******************************************************************************
   //******************************************************************************

   mError= SetParameterString (mSensor, "S_Command", "Set_Samplerate"); 
   mError= SetParameterDouble(mSensor, "SP_Measrate", mSampleRate); // 1 means 1.0 kHz err= SensorCommand (instance);mError = SensorCommand(mSensor);
   mError= SensorCommand (mSensor); 
   mError = GetError(mSensor, mErrorText, sizeof(mErrorText));
   Prn::print(Prn::ProcRun1,"Error %s", mErrorText);


   //******************************************************************************
   //******************************************************************************
   //******************************************************************************

   mError= SetParameterString (mSensor, "S_Command", "Set_DataOutInterface"); 
   mError= SetParameterInt(mSensor, "SP_DataOutInterface", 1); // 1 means set output. 
   mError= SensorCommand (mSensor); 
   mError = GetError(mSensor, mErrorText, sizeof(mErrorText));
   Prn::print(Prn::ProcRun1,"Error %s", mErrorText);


   //******************************************************************************
   //******************************************************************************
   //******************************************************************************

   mError= SetParameterString (mSensor, "S_Command", "Set_Output_RS422"); 
   mError= SetParameterInt(mSensor, "SP_OutputDistance1_RS422", 1); // 1 means set to distance measurement. 
   mError= SetParameterInt(mSensor, "SP_OutputAdditionalShutterTime_RS422", 1);
   mError= SetParameterInt(mSensor, "SP_OutputAdditionalCounter_RS422", 1);
   mError= SetParameterInt(mSensor, "SP_OutputAdditionalTimestamp_RS422", 0);
   mError= SetParameterInt(mSensor, "SP_OutputAdditionalIntensity_RS422", 0);
   mError= SetParameterInt(mSensor, "SP_OutputAdditionalState_RS422", 0);
   mError= SetParameterInt(mSensor, "SP_OutputAdditionalDistanceRaw_RS422", 1);
   mError= SetParameterInt(mSensor, "SP_OutputVideoRaw_RS422", 0);
   mError= SensorCommand (mSensor); 
   mError = GetError(mSensor, mErrorText, sizeof(mErrorText));
   Prn::print(Prn::ProcRun1,"Error %s", mErrorText);

}

//******************************************************************************
//******************************************************************************
//******************************************************************************
// Read the voltages at the IF2001 RS422 module.

void LP::lpLaser::read(double &dataOut)
{
   double average = 0.0;

   //******************************************************************************
   //******************************************************************************
   //******************************************************************************
   // check if data is available.
   const int tExpectedBlock = 100;
   int tCurrentlyAvailable = 0;
   while (tCurrentlyAvailable <= 100)
   {

      mError = DataAvail(mSensor, &tCurrentlyAvailable);
      Prn::print(Prn::ProcRun1, "Error %s", mErrorText);
      Prn::print(Prn::ProcRun1, "Currently Available %d", tCurrentlyAvailable);

   }

   while (true)
   {
      //******************************************************************************
      //******************************************************************************
      //******************************************************************************
      // if currently avilable data is greater than 100.
      if (tCurrentlyAvailable > tExpectedBlock)
      {

         //******************************************************************************
         //******************************************************************************
         //******************************************************************************
         // allocate memory to get raw and scaled data into.
         int tRawData[tExpectedBlock];
         double tScaledData[tExpectedBlock];

         //******************************************************************************
         //******************************************************************************
         //******************************************************************************
         // transfer data
         const int tExpectedBlockSize = 30;
         int tBlockSizeRecieved = 0;

         //******************************************************************************
         //******************************************************************************
         //******************************************************************************
         // Transfer the available data.
         mError = TransferData(mSensor, tRawData, tScaledData, tExpectedBlock, &tBlockSizeRecieved);
         Prn::print(Prn::ProcRun1, "Error %s", mErrorText);

         //******************************************************************************
         //******************************************************************************
         //******************************************************************************
         // Calculate the average.
         int n = 0;
         double sum = 0;
         for (int i = 3; i < tBlockSizeRecieved; i += 4)
         {
            Prn::print(Prn::ProcRun1, "Data %12f ", tScaledData[i]);
            n++;
            sum += tScaledData[i];
         }

         average = sum / n;

      }

      //******************************************************************************
      //******************************************************************************
      //******************************************************************************
      // Gaurd.
      if (std::isfinite(average) && average > 0)
      {
         break;
      }
      
   }

   dataOut = average;
}

//******************************************************************************
//******************************************************************************
//******************************************************************************
// Close the connection to the ILD1420.

void LP::lpLaser::close()
{

   // Close the connection to the laser. 
   // See section 6.7, page 79 of the medaqlib.pdf.
   mError = CloseSensor(mSensor);

   // Release the sensor instance.
   mError = ReleaseSensorInstance(mSensor);

}

//******************************************************************************
//******************************************************************************
//******************************************************************************
}//namespace