//******************************************************************************
//******************************************************************************
//******************************************************************************

#include "prnPrint.h"

#include "someQCallThread1.h"

#define  _SOMETWOTHREAD1_CPP_
#include "someTwoThread1.h"

#include "lpLabjack.h"
#include "lpLaser.h"
#include "lpFileWriter.h"
#include <ostream>
#include <iostream>
#include <fstream>
#include <iomanip>

namespace Some
{

//******************************************************************************
//******************************************************************************
//******************************************************************************
// Constructor.

TwoThread1::TwoThread1()
{
   // Set base class thread priority
   BaseClass::mShortThread->setThreadPriorityHigh();
   BaseClass::mLongThread->setThreadPriorityLow();

   // Set timer period
   BaseClass::mShortThread->mTimerPeriod = 1000;

   // Set call pointers
   BaseClass::mShortThread->mThreadInitCallPointer.bind(this, &TwoThread1::threadInitFunction);
   BaseClass::mShortThread->mThreadExitCallPointer.bind(this, &TwoThread1::threadExitFunction);
   BaseClass::mShortThread->mThreadExecuteOnTimerCallPointer.bind(this, &TwoThread1::executeOnTimer);

   // Set qcall call pointers.
   mDoSomething1QCall.bind (this->mLongThread,this,&TwoThread1::executeDoSomething1);
   mDoSomething2QCall.bind (this->mLongThread,this,&TwoThread1::executeDoSomething2);
}

//******************************************************************************
//******************************************************************************
//******************************************************************************
// Thread init function. This is called by the base class immedidately 
// after the thread starts running.

void TwoThread1::threadInitFunction()
{
   Prn::print(Prn::ProcInit1, "TwoThread1::threadInitFunction");
   LP::lpLaser laser;
   double tInitialDistance;

   laser.open();
   laser.read(tInitialDistance);
   laser.close();

   mZeroOffset = tInitialDistance;
}

//******************************************************************************
//******************************************************************************
//******************************************************************************
// Thread exit function. This is called by the base class immedidately
//  before the thread is terminated.

void TwoThread1::threadExitFunction()
{
   Prn::print(Prn::ProcInit1, "TwoThread1::threadExitFunction");
}

//******************************************************************************
//******************************************************************************
//******************************************************************************
// Execute periodically. This is called by the base class timer.

void TwoThread1::executeOnTimer (int aTimeCount)
{
   double tDistance = 0.0;
   double tVoltage = 0.0;
   double zeroOffset = 0.0;

   LP::lpFileWriter fileWriter;
   LP::lpLaser laser;
   LP::lpLabjack labjack;


   laser.open();
   laser.read(tDistance);
   laser.close();

   tDistance -= mZeroOffset;
   tDistance = -tDistance;
   Prn::print(Prn::ViewRun3, "Distance: %f mm", tDistance);

   fileWriter.appendLineToFile("C:/Temp/Log/distance.txt", tDistance);

   labjack.open();
   labjack.read(tVoltage);
   labjack.close();
   Prn::print(Prn::ViewRun1, "Voltage: %f V", tVoltage);

   fileWriter.appendLineToFile("C:/Temp/Log/voltage.txt", tVoltage);


   
}

//******************************************************************************
//******************************************************************************
//******************************************************************************
// Execute an example action. This is passed an example variable. 

void TwoThread1::executeDoSomething1(int aCode)
{
   double tData = 0.0;

   LP::lpLabjack labjack;

   labjack.open();
   labjack.read(tData);
   labjack.close();
   Prn::print(Prn::ViewRun1, "Voltage: %f V", tData);

   LP::lpFileWriter fileWriter;

   fileWriter.appendLineToFile("C:/Temp/Log/voltage.txt", tData);


}

//******************************************************************************
//******************************************************************************
//******************************************************************************
// Execute an example action. This is passed an example variable. 

void TwoThread1::executeDoSomething2(int aWaitTime)
{
   Prn::print(Prn::ViewRun1, "TwoThread::executeDoSomething2>>>>>>>>>>>BEGIN");

   //***************************************************************************
   //***************************************************************************
   //***************************************************************************
   // Execute example sequence: send a command to the qcall thread and wait
   // for a response.

   try
   {
      //************************************************************************
      //************************************************************************
      //************************************************************************
      // Locals.

      // Completion notification.
      Ris::Threads::TwoThreadNotify tNotify(this, 1);

      //************************************************************************
      //************************************************************************
      //************************************************************************
      // Show the target reference image.

      // Reset thread notifications.
      BaseClass::resetNotify();
           
      // Send a work request to the qcall thread.
      gQCallThread1->mRequestSomething1QCall(aWaitTime,&tNotify);

      // Wait for the notification, ten second timeout.
      BaseClass::waitForNotify(10000,1);
   }

   //***************************************************************************
   //***************************************************************************
   //***************************************************************************
   // If the preceding code section encountered errors or timeouts then 
   // this section executes. 

   catch (int aStatus)
   {
      Prn::print(Prn::ViewRun1, "Exception TwoThread::executeDoSomething2  ERROR %d",aStatus);

      // Exit.
      return;
   }

   //***************************************************************************
   //***************************************************************************
   //***************************************************************************
   // Done. 

   Prn::print(Prn::ViewRun1, "TwoThread::executeDoSomething2<<<<<<<<<<<END");
}

//******************************************************************************
//******************************************************************************
//******************************************************************************
}//namespace