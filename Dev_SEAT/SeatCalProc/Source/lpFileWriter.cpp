//******************************************************************************
//******************************************************************************
//******************************************************************************

#include "lpFileWriter.h"
#include <iostream>
#include <fstream>
#include <ostream>

namespace LP
{

//******************************************************************************
//******************************************************************************
//******************************************************************************
// Constructor.
lpFileWriter::lpFileWriter()
{
}

//******************************************************************************
//******************************************************************************
//******************************************************************************
// Write values of type double to a file string of format i.e: "C:/../file.txt".
void lpFileWriter::appendLineToFile(const char * aFileDirectory, double &aValue)
{
    std::ofstream file;
    //can't enable exception now because of gcc bug that raises ios_base::failure with useless message
    //file.exceptions(file.exceptions() | std::ios::failbit);
    file.open(aFileDirectory, std::ios::out | std::ios::app);
    if (file.fail())
        throw std::ios_base::failure(std::strerror(errno));

    //make sure write fails with exception if something is wrong
    file.exceptions(file.exceptions() | std::ios::failbit | std::ifstream::badbit);

    file << aValue << std::endl;
}


//******************************************************************************
//******************************************************************************
//******************************************************************************
}//namespace