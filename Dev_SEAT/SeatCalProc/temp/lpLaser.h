#pragma once
/*==============================================================================
Laser header file stores the methods and functions used to collect data from
the micro-epsilon IDL1420 laser sensor.
==============================================================================*/
//******************************************************************************
//******************************************************************************

#include "MEDAQLib.h"
#include "lpSerialObject.h"

//******************************************************************************
//******************************************************************************
//******************************************************************************

namespace LP
{

class LPlaser : public lpSerialObject
{
public:
   //***************************************************************************
   //***************************************************************************
   //***************************************************************************
   // Members.

   // Create an instance of the laser sensor driver.
   DWORD mSensor = CreateSensorInstance(SENSOR_ILD1420);
   DWORD mMaxLen = 200;

   ERR_CODE mError;

   char mErrorText[1024];
   char mString[1024];
   int mBaudrate = 921600;
   int mMeasureMode;
   double mMeasureRate;
   double mMasterVal = 0;
   double mSampleRate = 0.25;

   int mVideoTransmitted;
   int mOutputAdditionalShutter;
   int mAdditionalCounter;
   int mTimestampLo;
   int mTimestampHi;
   int mAdditionalIntensity;
   int mDistanceRaw = 0;
   int mDistanceLinearized;
   int mOutputDistance1;
   int mOutputAdditionalState;

    // Log file.
   static const int cLogLaser          = 0;

   //***************************************************************************
   //***************************************************************************
   //***************************************************************************
   // Intrastructure:

   // Constructor.
   LPlaser();

   //***************************************************************************
   //***************************************************************************
   //***************************************************************************
   // Methods

   // Open conection to the ILD1420 Sensor.
   void open() override;

   // Read the data from the IF2001 RS422 module.
   void read(double &dataOut) override;                   // Input\Output

   // Close the connection to the ILD1420.
   void close() override;

};

//******************************************************************************
//******************************************************************************
//******************************************************************************
}//namespace